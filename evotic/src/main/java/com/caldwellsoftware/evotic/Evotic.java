/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.caldwellsoftware.evotic;

import com.caldwellsoftware.dice.Dice;
import com.caldwellsoftware.dice.Dice.Builder;
import com.caldwellsoftware.dice.Gene;
import com.caldwellsoftware.dice.Genotype;
import com.caldwellsoftware.evotic.ResultRecorder.Record;
import com.caldwellsoftware.taman.TaSystem;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.commons.configuration.XMLConfiguration;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author bencaldwell
 */
public class Evotic {
    
    static Logger logger = LoggerFactory.getLogger(Evotic.class);
    
    private final File templateFile;
    private final File outputFolder;
    private final File configFile;
    private final File individualFile;
    private final File resultsFile;
    private final Dice dice;
    private final Random rand = new Random();
    private final ResultRecorder recorder;
    private int individualCounter=0;
    private int populationCounter=0;
    private int tScan=100; //PLC scan time (ms)
    private int tAdapter=500; // adapter latency time (ms)
    private int nMaxDepth=10; // maximum allowed depth for genotype tree
    private int nPopSize=10;
    private int nGenerations=10;
    private int verifyTimeOut=120; // wait for Xs for a verification
    private long randomSeed=0;
    private double mutationProbability = 0.1;
    private double argVariance = 0.1;
    private double parsimonyFitness = 1.0;
    private TaSystem currentTa = null;
    private File currentTaFile = null;
    private long startTime = 0;
        
    
    public Evotic(File configFile, File templateFile, File outputFolder) throws IOException {
        
        this.templateFile = templateFile;
        this.configFile = configFile;
        Date now = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("yyyyMMdd-HHmmss");
        this.outputFolder = new File(outputFolder, "evotic-"+ft.format(now));
        this.individualFile = new File(this.outputFolder, "individual.xml");
        this.resultsFile = new File(this.outputFolder, "results.csv");
        File templateCopy = new File(this.outputFolder, "template.xml");
        File configCopy = new File(this.outputFolder, "config.xml");
        
        // make output folder intermediate dirs if required
        this.outputFolder.mkdirs();
        // copy the template and config files to keep in the experiment archive
        Files.copy(templateFile.toPath(), templateCopy.toPath());
        Files.copy(configFile.toPath(), configCopy.toPath());
        // start the results recorder
        this.recorder = new ResultRecorder(this.resultsFile);
        
        String[] prefixList = null;
        List<Gene> inputs = new ArrayList<>();
        List<Gene> outputs = new ArrayList<>();
        List<Gene> genes = new ArrayList<>();
        
        Dice tempDice = null;
        try {
            // parse the config file to create IO and mappings
            XMLConfiguration config = new XMLConfiguration(configFile.getPath());
            
            tScan = config.getInt("PLC.T_SCAN");
            tAdapter = config.getInt("PLC.T_ADAPTER");
            nMaxDepth = config.getInt("GP.N_MAX_DEPTH");
            nPopSize = config.getInt("GP.N_POP_SIZE");
            nGenerations = config.getInt("GP.N_GENERATIONS");
            randomSeed = config.getLong("GP.RANDOM_SEED", new Random().nextLong());
            mutationProbability = config.getDouble("GP.INIT_MUTATION_PROBABILITY", mutationProbability);
            argVariance = config.getDouble("GP.INIT_ARGUMENT_VARIANCE", argVariance);
            parsimonyFitness = config.getDouble("GP.PARSIMONY_FITNESS", parsimonyFitness);
            
            // Get the prefix list for fully qualified tag names
            List<Object> prefixParms = config.getList("PREFIXLIST.PREFIX");
            prefixList = new String[prefixParms.size()];
            for (int i=0; i<prefixParms.size(); i++) {
                prefixList[i] = prefixParms.get(i).toString();
            }
                        
            // Get the inputs to the SUL, and create an input gene for each
            List<HierarchicalConfiguration> parms = config.configurationsAt("INPUTS.INPUT");
            int envIndex = 0;
            for (HierarchicalConfiguration parm : parms) {
                String tag = parm.getString("TAG");
                String type = parm.getString("TYPE");
                Map<String, String> constants = new HashMap<>();
                constants.put("TAG", tag);
                constants.put("TYPE", type);
                constants.put("ENVINDEX", Integer.toString(envIndex++)); // the environment generates random inputs based on index
                constants.put("INPUT",""); // flag this is as an input for special handling
                // input genes have arity=0, there is nothing connected to an input
                Gene gene = new Gene(tag, 0, false, constants, null, this::expressInput);
                inputs.add(gene);
            }
            
            // Get the outputs from the SUL, and create an output gene for each
            parms = config.configurationsAt("OUTPUTS.OUTPUT");
            envIndex = 0; //reset the environment index for separate output index scope
            for (HierarchicalConfiguration parm : parms) {
                String tag = parm.getString("TAG");
                String type = parm.getString("TYPE");
                Map<String, String> constants = new HashMap<>();
                constants.put("TAG", tag);
                constants.put("TYPE", type);
                constants.put("ENVINDEX", Integer.toString(envIndex++)); // the environment generates random inputs based on index
                // output genes have arity=1, there is one gene connected to an output
                Gene gene = new Gene(tag, 1, false, constants, null, this::expressOutput);
                outputs.add(gene);
            }
            
            // get the available genes from the SUL, these are copied into an expresion tree
            parms = config.configurationsAt("GENES.GENE");
            for (HierarchicalConfiguration parm : parms) {
                Map<String, String> constants = new HashMap<>();
                String name = parm.getString("NAME");
                int arity = parm.getInt("ARITY");
                boolean isCommutative = parm.getBoolean("COMMUTATIVE", Boolean.FALSE);
                constants.put("NAME", name);
                // get the args from the config file - may contain timer preset etc
                List<HierarchicalConfiguration> configArgs = parm.configurationsAt("PARAM");
                Map<String, Double> parameters = new HashMap<>();
                for (HierarchicalConfiguration configArg : configArgs) {
                    String paramName = configArg.getString("NAME");
                    Double paramValue = configArg.getDouble("VALUE");
                    parameters.put(paramName, paramValue);
                }
                // genes have arity=[1,unbounded], there is one output only from a gene
                Gene gene = new Gene(name, arity, isCommutative, constants, parameters, this::expressGene);
                genes.add(gene);
            }
            
            // create the Dice instance - this generates expression trees comprised of provided TA templates
            Builder builder = new Dice.Builder();
            builder.setStartPopulationExpression(this::startPopulationExpression);
            builder.setEndPopulationExpression(this::endPopulationExpression);
            builder.setStartIndividualExpression(this::startIndividualExpression);
            builder.setEndIndividualExpression(this::endIndividualExpression);
            builder.setStartIndividualEvaluation(this::startIndividualEvaluation);
            builder.setEndIndividualEvaluation(this::endIndividualEvaluation);
            builder.setPopSize(nPopSize);
            builder.setMaxIterations(nGenerations);
            builder.setMaxDepth(nMaxDepth);
            builder.setRandomSeed(randomSeed);
            builder.setMutationProbability(mutationProbability);
            builder.setArgVariance(argVariance);
            builder.setParsimonyFitness(parsimonyFitness);

            builder.addGenes(genes);
            builder.addInputs(inputs);
            builder.addOutputs(outputs);

            tempDice = builder.build();          
          
        } catch(Exception e) {
            logger.error("Failed to create configuration from config.xml: " + e);
            System.exit(1);
        } finally {
            dice = tempDice;
        }
    }
    
    public void loadParameters() throws ConfigurationException {
        // parse the config file to create IO and mappings
            XMLConfiguration config = new XMLConfiguration(configFile.getPath());
            
            tScan = config.getInt("PLC.T_SCAN", tScan);
            tAdapter = config.getInt("PLC.T_ADAPTER", tAdapter);
            nMaxDepth = config.getInt("GP.N_MAX_DEPTH", nMaxDepth);
            nPopSize = config.getInt("GP.N_POP_SIZE", nPopSize);
            nGenerations = config.getInt("GP.N_GENERATIONS", nGenerations);
            parsimonyFitness = config.getDouble("GP.PARSIMONY_FITNESS", parsimonyFitness);
            
            // don't change parameters that would affect evolved traits of current individuals e.g. argVariance
            
            dice.setMaxAllowedDepth(nMaxDepth);
            dice.setMaxIterations(nGenerations);
            dice.setPopSize(nPopSize);
            dice.setParsimonyFitness(parsimonyFitness);
    }
    
    public void run() throws Exception {
        dice.start();
    }
        
    public void startPopulationExpression(Void a) {
        System.out.println("Call startPopulation callback.");
        individualCounter = 0;
    }
    
    public void endPopulationExpression(Dice dice) {
        String output = String.format("Population# %1$d summary: avg fitness %2$.2f, avg gene count %3$.2f", populationCounter, dice.getAverageFitness(), dice.getAverageGeneCount());
        logger.debug(output);
        populationCounter++;
    }
    
    public void expressInput(Gene gene) {
        /* Merge inputs so there is only one adapter latency model per input
        this is done by giving them the same name and letting Taman prevent
        duplicates in the addInstance method.
        */
        String outVar = gene.getName()+"out";
        String inVar = gene.constants.get("TAG");
        String args = inVar + ", ";
        args = args + outVar;
        currentTa.addInstance("INPUT", gene.getUniqueName(), args);
        currentTa.addVarBool(outVar); // should already be in the template
        currentTa.addVarBool(inVar); // should already be in the template
    }
    
    public void expressOutput(Gene gene) {
        String outVar = gene.constants.get("TAG");
        String args = "";
        for (Gene input : gene.getInputs()) {
            if (input.constants.get("INPUT") == null) {
                args = args + input.getUniqueName()+"out";
            } else {
                args = args + input.getName()+"out";
            }
        }
        args = args + ", " + outVar;
        currentTa.addInstance("OUTPUT", gene.getUniqueName(), args);
        currentTa.addVarBool(outVar); // should already be in the template
    }
    
    public void expressGene(Gene gene) {
        String outVar = gene.getUniqueName()+"out";
        // args for a gene should be of the format "<exec channel>, <input>*, <preset>*"
        // execution order is greatest depth first, same depth can be executed at the same time
        String args = String.format("exec[%d]",gene.getDepthInverse() - 1);
        for (Gene input : gene.getInputs()) {
            if (input.constants.get("INPUT") == null) {
                args = args + ", " + input.getUniqueName()+"out";
            } else {
                args = args + ", " + input.getName()+"out";
            }
        }
        args = args + ", " + outVar;
        // add presets if they exist
        Double preset = gene.parameters.get("PRESET");
        if (preset != null) {
            args = args + ", " + preset.intValue();
        }
        currentTa.addInstance(gene.constants.get("NAME"), gene.getUniqueName(), args);
        currentTa.addVarBool(outVar);
    }
    
    public void startIndividualExpression(Genotype individual) {
        try {
            currentTa = new TaSystem(templateFile);
            currentTa.addConstInt("N_MAX_DEPTH", individual.getMaxDepth());
            currentTa.addConstInt("T_SCAN", tScan);
            currentTa.addConstInt("T_ADAPTER", tAdapter);
        } catch (Exception ex) {
            logger.error("Could not create a TA system: " + ex);
        }       
    }
    
    public void endIndividualExpression(Genotype individual) {
        logger.debug(String.format("Population: %1$d, Individual: %2$d %n %3$s", populationCounter, individualCounter, individual.printTree()));
        Path folder = this.individualFile.toPath().getParent();
        String fileName = FilenameUtils.getBaseName(this.individualFile.toString()); 
        fileName = String.format("%s-%03d-%03d", fileName, populationCounter, individualCounter);
        
        // write the uppaal file
        String uppaalFileName = fileName + "." + FilenameUtils.getExtension(this.individualFile.toString());
        Path path = folder.resolve(uppaalFileName);
        try {
            currentTaFile = path.toFile(); // retain the file path with the current TA in it - use this for fitness evaluation
            currentTa.Write(path.toFile());
        } catch (Exception ex) {
            logger.error("Could not write modified TA system to a file: " + ex);
        }

        // write the dot file representing the genotype graph
        String dotFileName = fileName + ".dot";
        path = folder.resolve(dotFileName);
        File dotFile = path.toFile();
        try (FileWriter writer = new FileWriter(dotFile, true)) {
            writer.write(individual.createGraph());
            writer.flush();
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(Evotic.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        individualCounter++;
    }
    
    public void startIndividualEvaluation(Genotype individual) {
        startTime = System.currentTimeMillis(); // start the clock
        Double fitness = 0.0;
        try {
            // start a process that calls the verifyta binary with the current individual's TA
            Process process = new ProcessBuilder("verifyta",currentTaFile.toString()).start();
            boolean verifyFinished = process.waitFor(verifyTimeOut, TimeUnit.SECONDS); // don't let the verifier run forever
            byte[] contents = new byte[1024];
            int bytesRead = 0;
            String verifyTaResponse = null; 
            while((bytesRead = process.getInputStream().read(contents)) != -1) { 
                verifyTaResponse += new String(contents, 0, bytesRead);
                if (!verifyFinished) {
                  process.destroy();
                  process.waitFor(); // wait for the process to terminate
                  break;
                }
            }
            
            /** Use regex to check for:
             * Positive: "-- Formula is satisfied"
             * Negative: "-- Formula is NOT satisfied"
             */
            // count negative matches
            double negCounter = 0;
            Pattern pattern = Pattern.compile("(.*)Formula(.*)NOT(.*)");
            Matcher matcher = pattern.matcher(verifyTaResponse);
            while (matcher.find()) {
                negCounter++;
            }
            // count all positive matches
            double posCounter = 0;
            pattern = Pattern.compile("(.*)Formula(?!.*NOT)(.*)");
            matcher = pattern.matcher(verifyTaResponse);
            while (matcher.find()) {
                posCounter++;
            }
            
            // fitness is the ratio of positive to all checks performed
            if (!verifyFinished) {
                fitness = 0.0; //TODO: give a fitness for the queries that were performed within timeout?
            } else {
                fitness = posCounter / (posCounter + negCounter);
            }
            
        } catch (Exception ex) {
            logger.error("Could not verify the TA: " + ex);
        }
        
        // update the fitness of the individual
        individual.setFitness(fitness);
    }
    
    public void endIndividualEvaluation(Genotype individual) {
        long evalTime = System.currentTimeMillis() - startTime;
        try {
            Record record = this.recorder.createRecord();
            record.setDate(new Date()).setRandomSeed(dice.getRandomSeed());
            record.setPopSize(dice.getPopSize()).setPopNumber(individual.getGenerationNum());
            record.setIndNumber(individual.getIndividualNum());
            record.setFitness(individual.getFitness()).setEvalTime(evalTime);
            record.setSize(individual.getGeneCount()).setMaxDepth(individual.getMaxDepth());
            record.setMutationProbability(individual.getMutationProbability()).setArgVariance(individual.getArgVariance());
            record.setExpression(individual.getExpression()); 
            
            this.recorder.writeRecord(record);

        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(Evotic.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void main(String[] args) throws ParseException {
        Options options = new Options();
        options.addOption("c", "config", true, "The config file describing inputs, outputs etc.");
        options.addOption("t", "template", true, "The uppaal system file containing the base template that will be manipulated according to config file.");
        options.addOption("o", "output", true, "The output folder to store experiment results in.");
        
        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = parser.parse(options, args);
        
        final File file = new File(".");
        System.out.println("file = " + file.getAbsoluteFile().getParent());
        
        //TODO: handle relative paths?
        try {
            File configFile = Paths.get(cmd.getOptionValue("c")).toFile();
            File templateFile = Paths.get(cmd.getOptionValue("t")).toFile();
            File outputFile = Paths.get(cmd.getOptionValue("o")).toFile();
            
            Evotic evotic = new Evotic( configFile, 
                                    templateFile,
                                    outputFile);
            evotic.run();
            while(true){
                Scanner sc = new Scanner(System.in);
                System.out.println("Run again with same population? Y/N (modify and save config before responding)");
                String response = sc.next();
                if (response.equalsIgnoreCase("N")) {
                    break;
                }
                evotic.loadParameters();
                evotic.run();
            }
        } catch (Exception e) {
            System.out.println(e);
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp( "evotic", options );
        }
    }
}
