/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.caldwellsoftware.evotic;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * ResultRecorder writes results of an individual to CSV for later analysis.
 * 
 * @author Ben Caldwell
 */
public class ResultRecorder {
    
    private final File csvFile;
    
    public class Record {
        private Date date;
        private long randomSeed;
        private int popSize;
        private int popNumber;
        private int indNumber;
        private double fitness;
        private long evalTime;
        private int size;
        private int maxDepth;
        private double mutationProbability;
        private double argVariance;
        private String expression;
        
        private Record() {
        }

        public Record setDate(Date date) {
            this.date = date;
            return this;
        }

        public Record setRandomSeed(long randomSeed) {
            this.randomSeed = randomSeed;
            return this;
        }

        public Record setPopSize(int popSize) {
            this.popSize = popSize;
            return this;
        }

        public Record setPopNumber(int popNumber) {
            this.popNumber = popNumber;
            return this;
        }

        public Record setIndNumber(int indNumber) {
            this.indNumber = indNumber;
            return this;
        }

        public Record setFitness(double fitness) {
            this.fitness = fitness;
            return this;
        }

        public Record setEvalTime(long evalTime) {
            this.evalTime = evalTime;
            return this;
        }

        public Record setSize(int size) {
            this.size = size;
            return this;
        }

        public Record setMaxDepth(int maxDepth) {
            this.maxDepth = maxDepth;
            return this;
        }

        public Record setMutationProbability(double mutationProbability) {
            this.mutationProbability = mutationProbability;
            return this;
        }

        public Record setArgVariance(double argVariance) {
            this.argVariance = argVariance;
            return this;
        }

        public Record setExpression(String expression) {
            this.expression = expression;
            return this;
        }     
        
    } 
            
    public ResultRecorder(File csvFile) throws IOException {
        this.csvFile = csvFile;
        try (FileWriter writer = new FileWriter(this.csvFile)) {
            writer.write("time,randomseed,populationsize,"
                    + "populationnumber,individual,fitness,"
                    + "evaltime,graphsize,maxdepth,mutationprobability,"
                    + "argvariance,expression"
                    +System.lineSeparator());
            writer.flush();
        }
    }
    
    /**
     * Get a new record object to populate data fields before writing.
     * 
     * @return 
     */
    public Record createRecord() {
        return new Record();
    }
    
    /**
     * Write one record row to the csv file.
     * 
     * @param date
     * @param popSize
     * @param popNumber
     * @param indNumber
     * @param fitness
     * @throws IOException 
     */
    public void writeRecord(Record record) throws IOException, IllegalStateException {
        if (record == null) {
            throw new IllegalStateException("Record is null.");
        }
        
        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");    
        String strRecord = String.format("%s,%d,%d,%d,%d,%.2f,%d,%d,%d,%.2f,%.2f,\"%s\""+System.lineSeparator(), 
                                        ft.format(record.date), 
                                        record.randomSeed,
                                        record.popSize,
                                        record.popNumber,
                                        record.indNumber,
                                        record.fitness,
                                        record.evalTime,
                                        record.size,
                                        record.maxDepth,
                                        record.mutationProbability,
                                        record.argVariance,
                                        record.expression);
        try (FileWriter writer = new FileWriter(this.csvFile, true)) {
            writer.write(strRecord);
            writer.flush();
        }
    }
    
}
