/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.caldwellsoftware.evotic;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;
import static org.testng.Assert.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author bencaldwell
 */
public class EvoticTest {
    
    public EvoticTest() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
    }
    
    @Test(enabled=false)
    public void digLatch() {
        try {
            File configFile = new File(this.getClass().getResource("diglatch/config.xml").toURI());
            File templateFile = new File(this.getClass().getResource("diglatch/template.xml").toURI());
            File outputFile = Paths.get(System.getProperty("user.dir"), "output", "test").toFile();
            
            Evotic evotic = new Evotic( configFile,
                                        templateFile,
                                        outputFile);
            
            evotic.run();
            
        } catch (Exception ex) {
            Logger.getLogger(EvoticTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
}
